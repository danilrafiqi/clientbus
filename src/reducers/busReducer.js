import { BUS_ALL } from '../actions/busActions'

const busReducer = (state=[], action) => {
	switch (action.type) {
		case BUS_ALL:
			return [...state, ...action.data]
		default:
			return state
	}
}


export default busReducer