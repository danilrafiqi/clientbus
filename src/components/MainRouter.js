import React, { Component } from 'react';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import { Switch, Route } from 'react-router-dom';
// import BusListNew from '../containers/bus/BusListNew';
import PoListNew from '../containers/po/PoListNew';
import RuteList from '../containers/rute/RuteList';
import BankList from '../containers/bank/BankList';
import KelasList from '../containers/kelas/KelasList';
import AgenList from '../containers/agen/AgenList';
import UserList from '../containers/user/UserList';
import Laporan from '../containers/dashboard/Laporan';
import AdminList from '../containers/admin/AdminList';
import BusList from '../containers/bus/BusList';
import KursiList from '../containers/kursi/KursiList';
import JadwalList from '../containers/jadwal/JadwalList';
import HargaList from '../containers/harga/HargaList';
import TiketList from '../containers/tiket/TiketList';
import TransaksiList from '../containers/transaksi/TransaksiList';
import PenumpangList from '../containers/penumpang/PenumpangList';
import AturanList from '../containers/aturan/AturanList';
import TujuanList from '../containers/tujuan/TujuanList';
import PemesanList from '../containers/pemesan/PemesanList';

const styles = theme => ({
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    height: '100vh',
    overflow: 'auto'
  }
});

class MainRouter extends Component {
  render() {
    const { classes } = this.props;
    return (
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Typography variant="display1" noWrap>
          <Switch>
            <Route exact={true} path="/" component={Laporan} />
          </Switch>

          <Switch>
            <Route path="/po" component={PoListNew} />
            <Route path="/aturan" component={AturanList} />
            <Route path="/kelas" component={KelasList} />
            <Route path="/bank" component={BankList} />
            <Route path="/rute" component={RuteList} />
            <Route path="/agen" component={AgenList} />
            <Route path="/user" component={UserList} />
            <Route path="/admin" component={AdminList} />
            <Route path="/bus" component={BusList} />
            <Route path="/kursi" component={KursiList} />
            <Route path="/transaksi" component={TransaksiList} />
            <Route path="/jadwal" component={JadwalList} />
            <Route path="/harga" component={HargaList} />
            <Route path="/tiket" component={TiketList} />

            <Route path="/penumpang" component={PenumpangList} />
            <Route path="/pemesan" component={PemesanList} />
            <Route path="/tujuan" component={TujuanList} />
            <Route path="/dashboard" component={Laporan} />
          </Switch>
        </Typography>
      </main>
    );
  }
}

export default withStyles(styles)(MainRouter);
