import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import DashboardIcon from '@material-ui/icons/Dashboard';
import PeopleIcon from '@material-ui/icons/People';
import DirectionsBus from '@material-ui/icons/DirectionsBus';
import Commute from '@material-ui/icons/Commute';
import Place from '@material-ui/icons/Place';
import Class from '@material-ui/icons/Class';
import AccountBalance from '@material-ui/icons/AccountBalance';
import AirlineSeatReclineNormal from '@material-ui/icons/AirlineSeatReclineNormal';
import Streetview from '@material-ui/icons/Streetview';
import SupervisedUserCircle from '@material-ui/icons/SupervisedUserCircle';
import AccountCircle from '@material-ui/icons/AccountCircle';
import PermIdentity from '@material-ui/icons/PermIdentity';
import Description from '@material-ui/icons/Description';
import EventSeat from '@material-ui/icons/EventSeat';
import Payment from '@material-ui/icons/Payment';
import ListAlt from '@material-ui/icons/ListAlt';
import Money from '@material-ui/icons/Money';
import { Link } from 'react-router-dom';

const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper
  },
  cleanLink: {
    textDecoration: 'none'
  }
});

const menu = [
  {
    link: '/po',
    icon: <Commute />,
    text: 'PO'
  },
  {
    link: '/rute',
    icon: <Place />,
    text: 'Rute'
  },
  {
    link: '/kelas',
    icon: <Class />,
    text: 'Kelas'
  },
  {
    link: '/penumpang',
    icon: <AirlineSeatReclineNormal />,
    text: 'Penumpang'
  },

  {
    link: '/aturan',
    icon: <Description />,
    text: 'Aturan'
  },

  {
    link: '/admin',
    icon: <AccountCircle />,
    text: 'Admin'
  },
  {
    link: '/tujuan',
    icon: <Streetview />,
    text: 'Tujuan'
  },
  {
    link: '/pemesan',
    icon: <PermIdentity />,
    text: 'Pemesan'
  },
  {
    link: '/bank',
    icon: <AccountBalance />,
    text: 'Bank'
  },
  {
    link: '/agen',
    icon: <PeopleIcon />,
    text: 'Agen'
  },
  {
    link: '/user',
    icon: <SupervisedUserCircle />,
    text: 'User'
  },
  {
    link: '/bus',
    icon: <DirectionsBus />,
    text: 'Bus'
  },
  {
    link: '/kursi',
    icon: <EventSeat />,
    text: 'Kursi'
  },
  {
    link: '/transaksi',
    icon: <Payment />,
    text: 'Transaksi'
  },
  {
    link: '/jadwal',
    icon: <ListAlt />,
    text: 'Jadwal'
  },
  {
    link: '/harga',
    icon: <Money />,
    text: 'Harga'
  },
  {
    link: '/tiket',
    icon: <DashboardIcon />,
    text: 'Tiket'
  },
  {
    link: '/dashboard',
    icon: <DashboardIcon />,
    text: 'Laporan'
  }
];

function ListMenu(props) {
  const { classes } = props;
  return (
    <div className={classes.root}>
      <Divider />
      <List component="nav">
        {menu.map((datas, index) => {
          return (
            <Link to={datas.link} className={classes.cleanLink} key={index}>
              <ListItem button>
                <ListItemIcon>{datas.icon}</ListItemIcon>
                <ListItemText primary={datas.text} />
              </ListItem>
            </Link>
          );
        })}
      </List>
    </div>
  );
}

ListMenu.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ListMenu);
