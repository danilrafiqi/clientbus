import React, { Component } from 'react';
import { Pie } from 'react-chartjs-2';
import axios from 'axios';

export default class PieChartComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Data: {}
    };
  }

  jumlahAgen = () => {
    axios.get(`http://localhost:2018/chart/jumlahagen`).then(res => {
      console.log('res', res.data);
      const jumlah_agen = res.data;
      let nama = [];
      let jumlah = [];
      jumlah_agen.forEach(element => {
        nama.push(element.nama);
        jumlah.push(element.jumlah);
      });
      this.setState({
        Data: {
          labels: nama,
          datasets: [
            {
              data: jumlah,
              backgroundColor: [
                'rgba(255,105,145,0.6)',
                'rgba(155,100,210,0.6)',
                'rgba(90,178,255,0.6)',
                'rgba(240,134,67,0.6)',
                'rgba(120,120,120,0.6)',
                'rgba(250,55,197,0.6)'
              ]
            }
          ]
        }
      });
    });
  };

  componentDidMount() {
    this.jumlahAgen();
  }
  render() {
    return (
      <div>
        <Pie data={this.state.Data} options={{ maintainAspectRatio: false }} />
      </div>
    );
  }
}
