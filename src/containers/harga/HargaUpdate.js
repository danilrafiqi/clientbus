import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import FilledInput from '@material-ui/core/FilledInput';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import axios from 'axios';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  }
});

class AlertDialog extends React.Component {
  state = {
    labelWidth: 0,
    open: false,
    id_harga: '',
    id_rute: '',
    id_tujuan: '',
    id_agen: '',
    data_rute: [],
    data_tujuan: [],
    data_agen: []
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  detail = id => {
    axios.get(`http://localhost:2018/harga/${id}`).then(res => {
      console.log('resnya', res.data);
      this.setState({
        open: true,
        id_rute: res.data[0].id_rute,
        id_tujuan: res.data[0].id_tujuan,
        id_agen: res.data[0].id_agen
      });
    });
  };

  getRute = () => {
    axios.get(`http://localhost:2018/rute/`).then(res => {
      this.setState({
        data_rute: res.data
      });
    });
  };

  getTujuan = () => {
    axios.get(`http://localhost:2018/tujuan/`).then(res => {
      this.setState({
        data_tujuan: res.data
      });
    });
  };

  getAgen = () => {
    axios.get(`http://localhost:2018/agen/`).then(res => {
      this.setState({
        data_agen: res.data
      });
    });
  };

  update = id => {
    axios
      .put(`http://localhost:2018/harga/${id}`, {
        id_rute: this.state.id_rute,
        id_tujuan: this.state.id_tujuan,
        id_agen: this.state.id_agen
      })
      .then(res => {
        this.setState({
          open: false,
          id_harga: '',
          id_rute: '',
          id_tujuan: '',
          id_agen: ''
        });
        this.props.getData();
      });
  };

  componentDidMount() {
    this.getTujuan();
    this.getAgen();
    this.getRute();
  }

  render() {
    const { classes } = this.props;

    return (
      <React.Fragment>
        <Button onClick={() => this.detail(this.props.idNya)}>Edit</Button>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description">
          <DialogTitle id="alert-dialog-title">
            {"Use Google's location service?"}
          </DialogTitle>
          <DialogContent>
            <form className={classes.root} autoComplete="off">
              <FormControl
                variant="filled"
                className={classes.formControl}
                fullWidth>
                <InputLabel htmlFor="id_rute">ID Rute</InputLabel>
                <Select
                  value={this.state.id_rute}
                  onChange={this.handleChange}
                  input={<FilledInput name="id_rute" id="id_rute" />}>
                  {this.state.data_rute.map(datas => {
                    return (
                      <MenuItem key={datas.id_rute} value={datas.id_rute}>
                        {datas.nama}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>

              <FormControl
                variant="filled"
                className={classes.formControl}
                fullWidth>
                <InputLabel htmlFor="id_tujuan">ID Tujuan</InputLabel>
                <Select
                  value={this.state.id_tujuan}
                  onChange={this.handleChange}
                  input={<FilledInput name="id_tujuan" id="id_tujuan" />}>
                  {this.state.data_tujuan.map(datas => {
                    return (
                      <MenuItem key={datas.id_tujuan} value={datas.id_tujuan}>
                        {datas.nama}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>

              <FormControl
                variant="filled"
                className={classes.formControl}
                fullWidth>
                <InputLabel htmlFor="id_agen">ID Agen</InputLabel>
                <Select
                  value={this.state.id_agen}
                  onChange={this.handleChange}
                  input={<FilledInput name="id_agen" id="id_agen" />}>
                  {this.state.data_agen.map(datas => {
                    return (
                      <MenuItem key={datas.id_agen} value={datas.id_agen}>
                        {datas.nama}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Batal
            </Button>
            <Button
              onClick={() => this.update(this.props.idNya)}
              color="primary"
              autoFocus>
              Simpan
            </Button>
          </DialogActions>
        </Dialog>
      </React.Fragment>
    );
  }
}

AlertDialog.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AlertDialog);
