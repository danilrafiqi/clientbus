import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import FilledInput from '@material-ui/core/FilledInput';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import axios from 'axios';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  }
});

class AlertDialog extends React.Component {
  state = {
    labelWidth: 0,
    open: false,
    id_bank: null,
    nama: null,
    no_rek: null,
    id_po: '',
    data_po: []
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  detail = id => {
    axios.get(`http://localhost:2018/bank/${id}`).then(res => {
      console.log('resnya', res.data);
      this.setState({
        nama: res.data[0].nama,
        no_rek: res.data[0].no_rek,
        id_po: res.data[0].id_po,
        open: true
      });
    });
  };

  getPo = () => {
    axios.get(`http://localhost:2018/po/`).then(res => {
      this.setState({
        data_po: res.data
      });
    });
  };
  update = id => {
    axios
      .put(`http://localhost:2018/bank/${id}`, {
        nama: this.state.nama,
        no_rek: this.state.no_rek,
        id_po: this.state.id_po
      })
      .then(res => {
        this.setState({
          open: false,
          id_bank: null,
          nama: '',
          no_rek: '',
          id_po: ''
        });
        this.props.getData();
      });
  };
  componentDidMount() {
    this.getPo();
  }
  render() {
    const { classes } = this.props;

    return (
      <React.Fragment>
        <Button onClick={() => this.detail(this.props.idNya)}>Edit</Button>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description">
          <DialogTitle id="alert-dialog-title">
            {"Use Google's location service?"}
          </DialogTitle>
          <DialogContent>
            <form className={classes.root} autoComplete="off">
              <TextField
                id="filled-name"
                label="Nama Bank"
                className={classes.textField}
                name="nama"
                value={this.state.nama}
                fullWidth
                onChange={this.handleChange}
                margin="normal"
                variant="filled"
              />
              <TextField
                id="filled-name"
                label="No Rekening"
                className={classes.textField}
                name="no_rek"
                value={this.state.no_rek}
                fullWidth
                onChange={this.handleChange}
                margin="normal"
                variant="filled"
              />

              <FormControl
                variant="filled"
                className={classes.formControl}
                fullWidth>
                <InputLabel htmlFor="id_po">PO</InputLabel>
                <Select
                  value={this.state.id_po}
                  onChange={this.handleChange}
                  input={<FilledInput name="id_po" id="id_po" />}>
                  {this.state.data_po.map(datas => {
                    return (
                      <MenuItem key={datas.id_po} value={datas.id_po}>
                        {datas.nama}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Batal
            </Button>
            <Button
              onClick={() => this.update(this.props.idNya)}
              color="primary"
              autoFocus>
              Simpan
            </Button>
          </DialogActions>
        </Dialog>
      </React.Fragment>
    );
  }
}

AlertDialog.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AlertDialog);
