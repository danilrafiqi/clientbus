import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import FilledInput from '@material-ui/core/FilledInput';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import axios from 'axios';
import moment from 'moment';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  }
});

class AlertDialog extends React.Component {
  state = {
    labelWidth: 0,
    open: false,
    id_jadwal: '',
    nama: '',
    tanggal_keberangkatan: '',
    waktu_keberangkatan: '',
    harga: '',
    no_plat: '',
    id_rute: '',
    data_bus: [],
    data_rute: []
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  getBus = () => {
    axios.get(`http://localhost:2018/bus/`).then(res => {
      this.setState({
        data_bus: res.data
      });
    });
  };

  getRute = () => {
    axios.get(`http://localhost:2018/rute/`).then(res => {
      this.setState({
        data_rute: res.data
      });
    });
  };

  update = id => {
    axios
      .put(`http://localhost:2018/jadwal/${id}`, {
        nama: this.state.nama,
        tanggal_keberangkatan: this.state.tanggal_keberangkatan,
        waktu_keberangkatan: this.state.waktu_keberangkatan,
        harga: this.state.harga,
        no_plat: this.state.no_plat,
        id_rute: this.state.id_rute
      })
      .then(res => {
        this.setState({
          open: false,
          id_jadwal: '',
          nama: '',
          tanggal_keberangkatan: '',
          waktu_keberangkatan: '',
          harga: '',
          no_plat: '',
          id_rute: ''
        });
        this.props.getData();
      });
  };
  detail = id => {
    axios.get(`http://localhost:2018/jadwal/${id}`).then(res => {
      this.setState({
        nama: res.data[0].nama,
        tanggal_keberangkatan: moment(res.data[0].tanggal_keberangkatan).format(
          'YYYY-MM-DD'
        ),
        waktu_keberangkatan: moment(res.data[0].waktu_keberangkatan).format(
          'LT'
        ),
        harga: res.data[0].harga,
        no_plat: res.data[0].no_plat,
        id_rute: res.data[0].id_rute,
        open: true
      });
    });
  };
  componentWillMount() {
    this.getBus();
    this.getRute();
  }

  render() {
    const { classes } = this.props;

    return (
      <React.Fragment>
        <Button onClick={() => this.detail(this.props.idNya)}>Edit</Button>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description">
          <DialogTitle id="alert-dialog-title">
            {"Use Google's location service?"}
          </DialogTitle>
          <DialogContent>
            <form className={classes.root} autoComplete="off">
              <TextField
                id="filled-name"
                label={'Nama'}
                className={classes.textField}
                name="nama"
                value={this.state.nama}
                fullWidth
                onChange={this.handleChange}
                margin="normal"
                variant="filled"
              />

              <TextField
                id="filled-name"
                label={'Harga'}
                className={classes.textField}
                name="harga"
                value={this.state.harga}
                fullWidth
                onChange={this.handleChange}
                margin="normal"
                variant="filled"
              />

              <TextField
                onChange={this.handleChange}
                id="date"
                label="Tanggal Keberangkatan"
                type="date"
                name="tanggal_keberangkatan"
                value={this.state.tanggal_keberangkatan}
                className={classes.textField}
                InputLabelProps={{
                  shrink: true
                }}
              />
              <TextField
                onChange={this.handleChange}
                id="time"
                label="Waktu Keberangkatan"
                type="time"
                name="waktu_keberangkatan"
                value={this.state.waktu_keberangkatan}
                className={classes.textField}
                InputLabelProps={{
                  shrink: true
                }}
              />

              <FormControl
                variant="filled"
                className={classes.formControl}
                fullWidth>
                <InputLabel htmlFor="no_plat">No Plat Bus</InputLabel>
                <Select
                  value={this.state.no_plat}
                  onChange={this.handleChange}
                  input={<FilledInput name="no_plat" id="no_plat" />}>
                  {this.state.data_bus.map(datas => {
                    return (
                      <MenuItem key={datas.no_plat} value={datas.no_plat}>
                        {datas.no_plat}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>

              <FormControl
                variant="filled"
                className={classes.formControl}
                fullWidth>
                <InputLabel htmlFor="id_rute">Rute</InputLabel>
                <Select
                  value={this.state.id_rute}
                  onChange={this.handleChange}
                  input={<FilledInput name="id_rute" id="id_rute" />}>
                  {this.state.data_rute.map(datas => {
                    return (
                      <MenuItem key={datas.id_rute} value={datas.id_rute}>
                        {datas.nama}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Batal
            </Button>
            <Button
              onClick={() => this.update(this.props.idNya)}
              color="primary"
              autoFocus>
              Simpan
            </Button>
          </DialogActions>
        </Dialog>
      </React.Fragment>
    );
  }
}

AlertDialog.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AlertDialog);
