import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

import axios from 'axios';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  }
});

class AlertDialog extends React.Component {
  state = {
    open: false,
    nama: '',
    no_hp: '',
    email: ''
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  update = id => {
    axios
      .put(`http://localhost:2018/penumpang/${id}`, {
        nama: this.state.nama,
        no_hp: this.state.no_hp,
        email: this.state.email
      })
      .then(res => {
        this.setState({
          open: false,
          nama: '',
          no_hp: '',
          email: ''
        });
        this.props.getData();
      });
  };
  detail = id => {
    axios.get(`http://localhost:2018/penumpang/${id}`).then(res => {
      console.log('resnya', res.data);
      this.setState({
        open: true,
        email: res.data[0].email,
        no_hp: res.data[0].no_hp,
        nama: res.data[0].nama
      });
    });
  };
  dataForm = [
    {
      title: 'Nama Penumpang',
      name: 'nama',
      nilai: this.state.nama
    },
    {
      title: 'No HP',
      name: 'no_hp',
      nilai: this.state.no_hp
    },
    {
      title: 'Email',
      name: 'email',
      nilai: this.state.email
    }
  ];

  render() {
    const { classes } = this.props;

    return (
      <React.Fragment>
        <Button onClick={() => this.detail(this.props.idNya)}>Edit</Button>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description">
          <DialogTitle id="alert-dialog-title">
            {"Use Google's location service?"}
          </DialogTitle>
          <DialogContent>
            <form className={classes.root} autoComplete="off">
              <TextField
                id="filled-name"
                label={'Nama'}
                className={classes.textField}
                name="nama"
                value={this.state.nama}
                fullWidth
                onChange={this.handleChange}
                margin="normal"
                variant="filled"
              />
              <TextField
                id="filled-name"
                label={'Email'}
                className={classes.textField}
                name="email"
                value={this.state.email}
                fullWidth
                onChange={this.handleChange}
                margin="normal"
                variant="filled"
              />
              <TextField
                id="filled-name"
                label={'No HP'}
                className={classes.textField}
                name="no_hp"
                value={this.state.no_hp}
                fullWidth
                onChange={this.handleChange}
                margin="normal"
                variant="filled"
              />
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Batal
            </Button>
            <Button
              onClick={() => this.update(this.props.idNya)}
              color="primary"
              autoFocus>
              Simpan
            </Button>
          </DialogActions>
        </Dialog>
      </React.Fragment>
    );
  }
}

AlertDialog.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AlertDialog);
