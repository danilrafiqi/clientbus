import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import FilledInput from '@material-ui/core/FilledInput';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import axios from 'axios';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  }
});

class AlertDialog extends React.Component {
  state = {
    labelWidth: 0,
    open: false,
    id_tiket: '',
    no_kursi: '',
    tgl_pemesanan: '',
    waktu_pemesanan: '',
    id_harga: '',
    id_jadwal: '',
    id_penumpang: '',
    id_transaksi: '',
    data_harga: [],
    data_jadwal: [],
    data_penumpang: [],
    data_transaksi: []
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  getHarga = () => {
    axios.get(`http://localhost:2018/harga/`).then(res => {
      this.setState({
        data_harga: res.data
      });
    });
  };

  getJadwal = () => {
    axios.get(`http://localhost:2018/jadwal/`).then(res => {
      this.setState({
        data_jadwal: res.data
      });
    });
  };

  getPenumpang = () => {
    axios.get(`http://localhost:2018/penumpang/`).then(res => {
      this.setState({
        data_penumpang: res.data
      });
    });
  };

  getTransaksi = () => {
    axios.get(`http://localhost:2018/transaksi/`).then(res => {
      this.setState({
        data_transaksi: res.data
      });
    });
  };

  create = () => {
    axios
      .post('http://localhost:2018/tiket', {
        no_kursi: this.state.no_kursi,
        tgl_pemesanan: this.state.tgl_pemesanan,
        waktu_pemesanan: this.state.waktu_pemesanan,
        id_harga: this.state.id_harga,
        id_jadwal: this.state.id_jadwal,
        id_penumpang: this.state.id_penumpang,
        id_transaksi: this.state.id_transaksi
      })
      .then(res => {
        this.setState({
          open: false,
          id_tiket: '',
          no_kursi: '',
          tgl_pemesanan: '',
          waktu_pemesanan: '',
          id_harga: '',
          id_jadwal: '',
          id_penumpang: '',
          id_transaksi: ''
        });
        this.props.getData();
      });
  };

  componentWillMount() {
    this.getHarga();
    this.getJadwal();
    this.getTransaksi();
    this.getPenumpang();
  }

  dataForm = [
    {
      title: 'No Kursi',
      name: 'no_kursi',
      nilai: this.state.no_kursi
    },
    {
      title: 'Tanggal Pemesanan',
      name: 'tgl_pemesanan',
      nilai: this.state.tgl_pemesanan
    },
    {
      title: 'Waktu Pemesanan',
      name: 'waktu_pemesanan',
      nilai: this.state.waktu_pemesanan
    }
  ];

  render() {
    const { classes } = this.props;

    return (
      <div>
        <Button onClick={this.handleClickOpen}>Add</Button>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description">
          <DialogTitle id="alert-dialog-title">
            {"Use Google's location service?"}
          </DialogTitle>
          <DialogContent>
            <form className={classes.root} autoComplete="off">
              {this.dataForm.map((datas, index) => {
                return (
                  <TextField
                    key={index}
                    id="filled-name"
                    label={datas.title}
                    className={classes.textField}
                    name={datas.name}
                    value={datas.value}
                    fullWidth
                    onChange={this.handleChange}
                    margin="normal"
                    variant="filled"
                  />
                );
              })}
              <FormControl
                variant="filled"
                className={classes.formControl}
                fullWidth>
                <InputLabel htmlFor="id_harga">Harga</InputLabel>
                <Select
                  value={this.state.id_harga}
                  onChange={this.handleChange}
                  input={<FilledInput name="id_harga" id="id_harga" />}>
                  {this.state.data_harga.map(datas => {
                    return (
                      <MenuItem key={datas.id_harga} value={datas.id_harga}>
                        {datas.nama}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>

              <FormControl
                variant="filled"
                className={classes.formControl}
                fullWidth>
                <InputLabel htmlFor="id_jadwal">Jadwal</InputLabel>
                <Select
                  value={this.state.id_jadwal}
                  onChange={this.handleChange}
                  input={<FilledInput name="id_jadwal" id="id_jadwal" />}>
                  {this.state.data_jadwal.map(datas => {
                    return (
                      <MenuItem key={datas.id_jadwal} value={datas.id_jadwal}>
                        {datas.nama}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>

              <FormControl
                variant="filled"
                className={classes.formControl}
                fullWidth>
                <InputLabel htmlFor="id_penumpang">Penumpang</InputLabel>
                <Select
                  value={this.state.id_penumpang}
                  onChange={this.handleChange}
                  input={<FilledInput name="id_penumpang" id="id_penumpang" />}>
                  {this.state.data_penumpang.map(datas => {
                    return (
                      <MenuItem
                        key={datas.id_penumpang}
                        value={datas.id_penumpang}>
                        {datas.nama}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>

              <FormControl
                variant="filled"
                className={classes.formControl}
                fullWidth>
                <InputLabel htmlFor="id_transaksi">Transaksi</InputLabel>
                <Select
                  value={this.state.id_transaksi}
                  onChange={this.handleChange}
                  input={<FilledInput name="id_transaksi" id="id_transaksi" />}>
                  {this.state.data_transaksi.map(datas => {
                    return (
                      <MenuItem
                        key={datas.id_transaksi}
                        value={datas.id_transaksi}>
                        {datas.nama}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Batal
            </Button>
            <Button onClick={this.create} color="primary" autoFocus>
              Simpan
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

AlertDialog.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AlertDialog);
