import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import FilledInput from '@material-ui/core/FilledInput';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import axios from 'axios';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  }
});

class AlertDialog extends React.Component {
  state = {
    labelWidth: 0,
    open: false,
    id_kursi: '',
    kode: '',
    no_plat: '',
    data_bus: []
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  detail = id => {
    axios.get(`http://localhost:2018/kursi/${id}`).then(res => {
      console.log('resnya', res.data);
      this.setState({
        open: true,
        kode: res.data[0].kode,
        no_plat: res.data[0].no_plat
      });
    });
  };

  getBus = () => {
    axios.get(`http://localhost:2018/bus/`).then(res => {
      this.setState({
        data_bus: res.data
      });
    });
  };
  update = id => {
    axios
      .put(`http://localhost:2018/kursi/${id}`, {
        kode: this.state.kode,
        no_plat: this.state.no_plat
      })
      .then(res => {
        this.setState({
          open: false,
          id_kursi: '',
          kode: '',
          no_plat: ''
        });
        this.props.getData();
      });
  };
  componentDidMount() {
    this.getBus();
  }
  render() {
    const { classes } = this.props;

    return (
      <React.Fragment>
        <Button onClick={() => this.detail(this.props.idNya)}>Edit</Button>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description">
          <DialogTitle id="alert-dialog-title">
            {"Use Google's location service?"}
          </DialogTitle>
          <DialogContent>
            <form className={classes.root} autoComplete="off">
              <TextField
                id="filled-name"
                label="Kode Kursi"
                className={classes.textField}
                name="kode"
                value={this.state.kode}
                fullWidth
                onChange={this.handleChange}
                margin="normal"
                variant="filled"
              />
              <FormControl
                variant="filled"
                className={classes.formControl}
                fullWidth>
                <InputLabel htmlFor="no_plat">No Plat</InputLabel>
                <Select
                  value={this.state.no_plat}
                  onChange={this.handleChange}
                  input={<FilledInput name="no_plat" id="no_plat" />}>
                  {this.state.data_bus.map(datas => {
                    return (
                      <MenuItem key={datas.no_plat} value={datas.no_plat}>
                        {datas.no_plat}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Batal
            </Button>
            <Button
              onClick={() => this.update(this.props.idNya)}
              color="primary"
              autoFocus>
              Simpan
            </Button>
          </DialogActions>
        </Dialog>
      </React.Fragment>
    );
  }
}

AlertDialog.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AlertDialog);
