import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import FilledInput from '@material-ui/core/FilledInput';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import axios from 'axios';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  }
});

class AlertDialog extends React.Component {
  state = {
    labelWidth: 0,
    open: false,
    no_plat: '',
    status: '',
    jumlah_kursi: '',
    tipe_kursi: '',
    id_kelas: '',
    id_po: '',
    data_kelas: [],
    data_po: []
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  detail = id => {
    axios.get(`http://localhost:2018/bus/${id}`).then(res => {
      console.log('resnya', res.data);
      this.setState({
        open: true,
        status: res.data[0].status,
        jumlah_kursi: res.data[0].jumlah_kursi,
        tipe_kursi: res.data[0].tipe_kursi,
        id_kelas: res.data[0].id_kelas,
        id_po: res.data[0].id_po,
        no_plat: res.data[0].no_plat
      });
    });
  };

  getKelas = () => {
    axios.get(`http://localhost:2018/kelas/`).then(res => {
      this.setState({
        data_kelas: res.data
      });
    });
  };

  getPo = () => {
    axios.get(`http://localhost:2018/po/`).then(res => {
      this.setState({
        data_po: res.data
      });
    });
  };

  update = id => {
    axios
      .put(`http://localhost:2018/bus/${id}`, {
        status: this.state.status,
        jumlah_kursi: this.state.jumlah_kursi,
        tipe_kursi: this.state.tipe_kursi,
        id_kelas: this.state.id_kelas,
        id_po: this.state.id_po
      })
      .then(res => {
        this.setState({
          open: false,
          no_plat: '',
          status: '',
          jumlah_kursi: '',
          tipe_kursi: '',
          id_kelas: '',
          id_po: ''
        });
        this.props.getData();
      });
  };
  componentDidMount() {
    this.getPo();
    this.getKelas();
  }
  render() {
    const { classes } = this.props;

    return (
      <React.Fragment>
        <Button onClick={() => this.detail(this.props.idNya)}>Edit</Button>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description">
          <DialogTitle id="alert-dialog-title">
            {"Use Google's location service?"}
          </DialogTitle>
          <DialogContent>
            <form className={classes.root} autoComplete="off">
              <TextField
                id="filled-name"
                label="No Plat"
                className={classes.textField}
                name="no_plat"
                value={this.state.no_plat}
                fullWidth
                onChange={this.handleChange}
                margin="normal"
                variant="filled"
              />

              <TextField
                id="filled-name"
                label="Nama Status"
                className={classes.textField}
                name="status"
                value={this.state.status}
                fullWidth
                onChange={this.handleChange}
                margin="normal"
                variant="filled"
              />
              <TextField
                id="filled-name"
                label="Jumlah Kursi"
                className={classes.textField}
                name="jumlah_kursi"
                value={this.state.jumlah_kursi}
                fullWidth
                onChange={this.handleChange}
                margin="normal"
                variant="filled"
              />
              <TextField
                id="filled-name"
                label="Tipe Kursi"
                className={classes.textField}
                name="tipe_kursi"
                value={this.state.tipe_kursi}
                fullWidth
                onChange={this.handleChange}
                margin="normal"
                variant="filled"
              />

              <FormControl
                variant="filled"
                className={classes.formControl}
                fullWidth>
                <InputLabel htmlFor="id_kelas">Kelas</InputLabel>
                <Select
                  value={this.state.id_kelas}
                  onChange={this.handleChange}
                  input={<FilledInput name="id_kelas" id="id_kelas" />}>
                  {this.state.data_kelas.map(datas => {
                    return (
                      <MenuItem key={datas.id_kelas} value={datas.id_kelas}>
                        {datas.nama_kelas}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>

              <FormControl
                variant="filled"
                className={classes.formControl}
                fullWidth>
                <InputLabel htmlFor="id_po">PO</InputLabel>
                <Select
                  value={this.state.id_po}
                  onChange={this.handleChange}
                  input={<FilledInput name="id_po" id="id_po" />}>
                  {this.state.data_po.map(datas => {
                    return (
                      <MenuItem key={datas.id_po} value={datas.id_po}>
                        {datas.nama}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Batal
            </Button>
            <Button
              onClick={() => this.update(this.props.idNya)}
              color="primary"
              autoFocus>
              Simpan
            </Button>
          </DialogActions>
        </Dialog>
      </React.Fragment>
    );
  }
}

AlertDialog.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AlertDialog);
