import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

import Modal from '@material-ui/core/Modal';
import Button from '@material-ui/core/Button';

import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';

import AddIcon from '@material-ui/icons/Add';
import SaveIcon from '@material-ui/icons/Save';
import axios from 'axios';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';

const getModalStyle = () => {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`
  };
};

const styles = theme => ({
  paper: {
    position: 'absolute',
    width: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4
  },
  //style inputan
  container: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  formControl: {
    margin: theme.spacing.unit
  },

  button: {
    margin: theme.spacing.unit
  },
  rightIcon: {
    marginLeft: theme.spacing.unit
  }
});

class Add extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      nama_kelas: null,
      deskripsi: null,
      harga: null
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.create = this.create.bind(this);

    this.dataForm = [
      {
        title: 'Nama Kelas',
        name: 'nama_kelas',
        nilai: this.state.nama_kelas
      },
      {
        title: 'Deskripsi',
        name: 'deskripsi',
        nilai: this.state.deskripsi
      },
      {
        title: 'Harga',
        name: 'harga',
        nilai: this.state.harga
      }
    ];
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  handleOpen() {
    this.setState({ open: true });
  }

  handleClose() {
    this.setState({ open: false });
  }

  create() {
    axios
      .post('http://localhost:2018/kelas', {
        nama_kelas: this.state.nama_kelas,
        deskripsi: this.state.deskripsi,
        harga: this.state.harga
      })
      .then(res => {
        this.setState({
          open: false,
          nama_kelas: null,
          deskripsi: null,
          harga: null
        });
        this.props.getData();
      });
  }

  render() {
    const { classes } = this.props;

    return (
      <div>
        <Tooltip title="Add" onClick={this.handleOpen}>
          <IconButton aria-label="Add">
            <AddIcon />
          </IconButton>
        </Tooltip>

        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.state.open}
          onClose={this.handleClose}>
          <div style={getModalStyle()} className={classes.paper}>
            <Typography variant="title" id="modal-title">
              Add Data
            </Typography>
            <br />
            <Typography variant="subheading" id="simple-modal-description">
              <div className={classes.container}>
                {this.dataForm.map((datas, index) => {
                  return (
                    <FormControl className={classes.formControl} key={index}>
                      <InputLabel htmlFor="">{datas.title}</InputLabel>
                      <Input
                        id=""
                        name={datas.name}
                        value={datas.value}
                        fullWidth={true}
                        onChange={this.handleChange}
                      />
                    </FormControl>
                  );
                })}
              </div>
            </Typography>

            <Button
              onClick={this.create}
              className={classes.button}
              variant="contained"
              color="secondary">
              Save
              <SaveIcon className={classes.rightIcon} />
            </Button>
          </div>
        </Modal>
      </div>
    );
  }
}

Add.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Add);
