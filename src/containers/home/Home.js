import React, { Component } from 'react';
import Header from '../../components/home/Header';
import SearchSlider from '../../components/home/SearchSlider';
import FormCari from '../../components/home/FormCari';
import Slider from '../../components/home/Slider';
class Home extends Component {
  render() {
    return (
      <React.Fragment>
        <Header />
        <br />
        <SearchSlider kiri={<FormCari />} kanan={<Slider />} />
      </React.Fragment>
    );
  }
}

export default Home;
