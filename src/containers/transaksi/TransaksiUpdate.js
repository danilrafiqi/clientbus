import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import FilledInput from '@material-ui/core/FilledInput';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import axios from 'axios';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  }
});

class AlertDialog extends React.Component {
  state = {
    labelWidth: 0,
    open: false,
    id_transaksi: '',
    bukti_pembayaran: '',
    status_transaksi: '',
    total: '',
    tgl_pesan: '',
    expired: '',
    id_pemesan: '',
    data_pemesan: []
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  detail = id => {
    axios.get(`http://localhost:2018/transaksi/${id}`).then(res => {
      console.log('resnya', res.data);
      this.setState({
        open: true,
        bukti_pembayaran: res.data[0].bukti_pembayaran,
        status_transaksi: res.data[0].status_transaksi,
        total: res.data[0].total,
        tgl_pesan: res.data[0].tgl_pesan,
        expired: res.data[0].expired,
        id_pemesan: res.data[0].id_pemesan
      });
    });
  };

  getPemesan = () => {
    axios.get(`http://localhost:2018/pemesan/`).then(res => {
      this.setState({
        data_pemesan: res.data
      });
    });
  };
  update = id => {
    axios
      .put(`http://localhost:2018/transaksi/${id}`, {
        bukti_pembayaran: this.state.bukti_pembayaran,
        status_transaksi: this.state.status_transaksi,
        total: this.state.total,
        tgl_pesan: this.state.tgl_pesan,
        expired: this.state.expired,
        id_pemesan: this.state.id_pemesan
      })
      .then(res => {
        this.setState({
          open: false,
          id_transaksi: '',
          bukti_pembayaran: '',
          status_transaksi: '',
          total: '',
          tgl_pesan: '',
          expired: '',
          id_pemesan: ''
        });
        this.props.getData();
      });
  };
  componentDidMount() {
    this.getPemesan();
  }
  render() {
    const { classes } = this.props;

    return (
      <React.Fragment>
        <Button onClick={() => this.detail(this.props.idNya)}>Edit</Button>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description">
          <DialogTitle id="alert-dialog-title">
            {"Use Google's location service?"}
          </DialogTitle>
          <DialogContent>
            <form className={classes.root} autoComplete="off">
              <TextField
                id="filled-name"
                label="Bukti Pembayaran"
                className={classes.textField}
                name="bukti_pembayaran"
                value={this.state.bukti_pembayaran}
                fullWidth
                onChange={this.handleChange}
                margin="normal"
                variant="filled"
              />

              <FormControl
                variant="filled"
                className={classes.formControl}
                fullWidth>
                <InputLabel htmlFor="status_transaksi">
                  {' '}
                  Status Transaksi
                </InputLabel>

                <Select
                  value={this.state.status_transaksi}
                  onChange={this.handleChange}
                  input={
                    <FilledInput
                      name="status_transaksi"
                      id="status_transaksi"
                    />
                  }>
                  <MenuItem value="lunas">Lunas</MenuItem>{' '}
                  <MenuItem value="kurang"> Kurang</MenuItem>
                  <MenuItem value="gagal"> Gagal </MenuItem> })}
                </Select>
              </FormControl>

              <TextField
                id="filled-name"
                label="Total"
                className={classes.textField}
                name="total"
                value={this.state.total}
                fullWidth
                onChange={this.handleChange}
                margin="normal"
                variant="filled"
              />

              <TextField
                id="filled-name"
                label="Tanggal Pesan"
                className={classes.textField}
                name="tgl_pesan"
                value={this.state.tgl_pesan}
                fullWidth
                onChange={this.handleChange}
                margin="normal"
                variant="filled"
              />

              <TextField
                id="filled-name"
                label="Expired"
                className={classes.textField}
                name="expired"
                value={this.state.expired}
                fullWidth
                onChange={this.handleChange}
                margin="normal"
                variant="filled"
              />

              <FormControl
                variant="filled"
                className={classes.formControl}
                fullWidth>
                <InputLabel htmlFor="id_pemesan">Pemesan</InputLabel>
                <Select
                  value={this.state.id_pemesan}
                  onChange={this.handleChange}
                  input={<FilledInput name="id_pemesan" id="id_pemesan" />}>
                  {this.state.data_pemesan.map(datas => {
                    return (
                      <MenuItem key={datas.id_pemesan} value={datas.id_pemesan}>
                        {datas.nama}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Batal
            </Button>
            <Button
              onClick={() => this.update(this.props.idNya)}
              color="primary"
              autoFocus>
              Simpan
            </Button>
          </DialogActions>
        </Dialog>
      </React.Fragment>
    );
  }
}

AlertDialog.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AlertDialog);
